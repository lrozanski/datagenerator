package com.lrozanski.datagenerator.util;

import com.lrozanski.datagenerator.exception.DataGeneratorException;
import com.lrozanski.datagenerator.exception.NewClassInstanceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ClassUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassUtils.class);

    private ClassUtils() {
    }

    public static <T> T createInstance(Class<T> targetClass) {
        LOGGER.debug("Creating instance of class: {}", targetClass);
        try {
            return targetClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new NewClassInstanceException(e);
        }
    }

    public static Class getClassForName(String className) {
        LOGGER.debug("Resolving class name: {}", className);
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new DataGeneratorException(e);
        }
    }

    public static Set<Method> getAllDeclaredMethods(final Class targetClass) {
        LOGGER.debug("Retrieving all declared methods for class: {}", targetClass);
        final Set<Method> declaredMethods = Arrays.stream(targetClass.getDeclaredMethods()).collect(
                Collectors.toCollection(HashSet::new));
        final Class superClass = targetClass.getSuperclass();
        if (superClass != null && !Object.class.equals(superClass)) {
            declaredMethods.addAll(getAllDeclaredMethods(superClass));
        }
        return declaredMethods;
    }

    public static Set<Method> filterSetters(final Set<Method> allMethods) {
        LOGGER.debug("Filtering setters");
        return allMethods.stream().filter(method -> !method.isSynthetic() && method.getName().startsWith("set"))
                         .collect(Collectors.toCollection(HashSet::new));
    }
}
