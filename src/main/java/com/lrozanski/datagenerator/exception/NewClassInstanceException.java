package com.lrozanski.datagenerator.exception;

public class NewClassInstanceException extends DataGeneratorException {
    public NewClassInstanceException() {
    }

    public NewClassInstanceException(String message) {
        super(message);
    }

    public NewClassInstanceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NewClassInstanceException(Throwable cause) {
        super(cause);
    }
}
