package com.lrozanski.datagenerator.exception;

public class DataGeneratorException extends RuntimeException {
    public DataGeneratorException() {
    }

    public DataGeneratorException(String message) {
        super(message);
    }

    public DataGeneratorException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataGeneratorException(Throwable cause) {
        super(cause);
    }
}
