package com.lrozanski.datagenerator.exception;

public class NoGeneratorForTypeException extends DataGeneratorException {

    public NoGeneratorForTypeException(Class type) {
        super("Generator for type " + type + " not found");
    }
}
