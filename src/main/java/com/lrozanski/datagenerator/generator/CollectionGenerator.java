package com.lrozanski.datagenerator.generator;

import java.util.Collection;
import java.util.OptionalLong;

@FunctionalInterface
public interface CollectionGenerator<T> extends Generator<Collection<T>> {

    default Collection<T> generateValue() {
        return generateValue(OptionalLong.empty());
    }

    Collection<T> generateValue(OptionalLong seed);
}
