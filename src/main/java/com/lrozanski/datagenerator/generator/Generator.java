package com.lrozanski.datagenerator.generator;

import java.util.OptionalLong;

@FunctionalInterface
public interface Generator<T> {

    default T generateValue() {
        return generateValue(OptionalLong.empty());
    }

    T generateValue(OptionalLong seed);
}
