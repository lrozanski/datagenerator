package com.lrozanski.datagenerator.generator.basic;

import com.lrozanski.datagenerator.generator.Generator;

import java.util.Date;
import java.util.OptionalLong;

public class BasicDateGenerator implements Generator<Date> {

    @Override
    public Date generateValue(OptionalLong seed) {
        return new Date();
    }
}
