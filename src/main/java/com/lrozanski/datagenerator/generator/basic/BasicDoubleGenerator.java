package com.lrozanski.datagenerator.generator.basic;

import com.lrozanski.datagenerator.generator.Generator;

import java.util.OptionalLong;
import java.util.Random;

public class BasicDoubleGenerator implements Generator<Double> {

    @Override
    public Double generateValue(final OptionalLong seed) {
        Random random = (seed.isPresent()) ? new Random(seed.getAsLong()) : new Random();
        return random.nextDouble();
    }
}
