package com.lrozanski.datagenerator.generator.basic;

import com.lrozanski.datagenerator.generator.CollectionGenerator;
import com.lrozanski.datagenerator.generator.Generator;

import java.util.HashSet;
import java.util.OptionalLong;
import java.util.Random;
import java.util.Set;
import java.util.function.IntSupplier;

public class BasicSetGenerator<T> implements CollectionGenerator<T> {

    private static final int MIN_ELEMENTS = 1;
    private static final int MAX_ELEMENTS = 3;

    private Generator<T> elementGenerator;
    private IntSupplier countSupplier;

    @SuppressWarnings("unchecked")
    public BasicSetGenerator(Generator<T> elementGenerator) {
        this.elementGenerator = elementGenerator;
    }

    public BasicSetGenerator(Generator<T> elementGenerator, IntSupplier countSupplier) {
        this.elementGenerator = elementGenerator;
        this.countSupplier = countSupplier;
    }

    @Override
    public Set<T> generateValue(OptionalLong seed) {
        Set<T> list = new HashSet<>();

        Random random = (seed.isPresent()) ? new Random(seed.getAsLong()) : new Random();
        int count = (countSupplier == null)
                ? random.nextInt((MAX_ELEMENTS + 1) - MIN_ELEMENTS) + MIN_ELEMENTS
                : countSupplier.getAsInt();
        for (int i = 0; i < count; i++) {
            @SuppressWarnings("unchecked")
            T value = elementGenerator.generateValue(seed);
            list.add(value);
        }
        return list;
    }
}
