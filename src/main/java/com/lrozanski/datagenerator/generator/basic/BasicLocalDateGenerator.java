package com.lrozanski.datagenerator.generator.basic;

import com.lrozanski.datagenerator.generator.Generator;

import java.time.LocalDate;
import java.util.OptionalLong;

public class BasicLocalDateGenerator implements Generator<LocalDate> {

    @Override
    public LocalDate generateValue(OptionalLong seed) {
        return LocalDate.now();
    }
}
