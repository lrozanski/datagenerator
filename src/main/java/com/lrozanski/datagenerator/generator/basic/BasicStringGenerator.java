package com.lrozanski.datagenerator.generator.basic;

import com.lrozanski.datagenerator.generator.Generator;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.OptionalLong;
import java.util.Random;

public class BasicStringGenerator implements Generator<String> {

    private static final int count = 255;

    private boolean useLetterChars = true;
    private boolean useNumberChars = true;
    private boolean userOtherChars = true;

    public boolean isUseLetterChars() {
        return useLetterChars;
    }

    public void setUseLetterChars(boolean useLetterChars) {
        this.useLetterChars = useLetterChars;
    }

    public boolean isUseNumberChars() {
        return useNumberChars;
    }

    public void setUseNumberChars(boolean useNumberChars) {
        this.useNumberChars = useNumberChars;
    }

    public boolean isUserOtherChars() {
        return userOtherChars;
    }

    public void setUserOtherChars(boolean userOtherChars) {
        this.userOtherChars = userOtherChars;
    }

    @Override
    public String generateValue(final OptionalLong seed) {
        Random random = (seed.isPresent()) ? new Random(seed.getAsLong()) : new Random();
        if (userOtherChars) {
            return RandomStringUtils.random(count, 32, 126, false, false, null, random);
        }
        return RandomStringUtils.random(count, 32, 126, useLetterChars, useNumberChars, null, random);
    }
}
