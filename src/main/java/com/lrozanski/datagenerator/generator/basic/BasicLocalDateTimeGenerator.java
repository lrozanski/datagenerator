package com.lrozanski.datagenerator.generator.basic;

import com.lrozanski.datagenerator.generator.Generator;

import java.time.LocalDateTime;
import java.util.OptionalLong;

public class BasicLocalDateTimeGenerator implements Generator<LocalDateTime> {

    @Override
    public LocalDateTime generateValue(OptionalLong seed) {
        return LocalDateTime.now();
    }
}
