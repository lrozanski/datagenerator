package com.lrozanski.datagenerator;

import com.lrozanski.datagenerator.exception.DataGeneratorException;
import com.lrozanski.datagenerator.exception.NoGeneratorForTypeException;
import com.lrozanski.datagenerator.generator.Generator;
import com.lrozanski.datagenerator.util.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

@SuppressWarnings("WeakerAccess")
public class DataGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataGenerator.class);

    private Configuration configuration;

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public <T> T generateForClass(final Class<T> targetClass) {
        LOGGER.info(String.format("Creating a new instance of class '%s' populated with generated data", targetClass.getCanonicalName()));
        final T target = ClassUtils.createInstance(targetClass);
        generateValuesForTargetClass(target, targetClass);
        return target;
    }

    public void generateForClass(final Object target, final Class targetClass) {
        LOGGER.info(String.format("Populating target object of class '%s' with generated data", targetClass.getCanonicalName()));
        generateValuesForTargetClass(target, targetClass);
    }

    private void generateValuesForTargetClass(Object target, Class targetClass) {
        final Set<Method> allMethods = ClassUtils.getAllDeclaredMethods(targetClass);
        final Set<Method> allSetters = ClassUtils.filterSetters(allMethods);

        for (Method setter : allSetters) {
            final Optional<Generator> generator = resolveGeneratorForSetter(setter);
            if (!generator.isPresent()) {
                continue;
            }

            final Object value = generator.get().generateValue();
            try {
                setter.invoke(target, value);
                final String fieldName = StringUtils.uncapitalize(setter.getName().replace("set", ""));
                LOGGER.trace("New value generated for field '{}': {}", fieldName, value);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new DataGeneratorException(e);
            }
        }
    }

    private Optional<Generator> resolveGeneratorForSetter(Method setter) {
        if (setter.getParameterCount() != 1) {
            return Optional.empty();
        }
        Class returnType = setter.getParameterTypes()[0];
        LOGGER.debug("Parameter type: {}", returnType);
        try {
            if (Collection.class.isAssignableFrom(returnType)) {
                Type type = ((ParameterizedType) setter.getGenericParameterTypes()[0]).getActualTypeArguments()[0];
                LOGGER.debug("Generic parameter type: {}", type);
                Class elementClass = ClassUtils.getClassForName(type.getTypeName());
                Generator collectionGenerator = configuration.getCollectionGeneratorForType(returnType, elementClass);
                return Optional.of(collectionGenerator);
            }
            Generator generator = configuration.getGeneratorForType(returnType);
            return Optional.of(generator);
        } catch (NoGeneratorForTypeException e) {
            LOGGER.info("No generator found for class: {}, skipping", returnType);
            return Optional.empty();
        }
    }
}
