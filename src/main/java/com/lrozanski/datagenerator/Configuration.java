package com.lrozanski.datagenerator;

import com.lrozanski.datagenerator.exception.NoGeneratorForTypeException;
import com.lrozanski.datagenerator.generator.CollectionGenerator;
import com.lrozanski.datagenerator.generator.Generator;

import java.io.Serializable;
import java.util.Map;

@SuppressWarnings("WeakerAccess")
public class Configuration implements Serializable {

    private static final long serialVersionUID = 549117035340668453L;

    private Map<String, Generator> generators;
    private Map<String, Map<String, CollectionGenerator>> collectionGenerators;

    public Map<String, Generator> getGenerators() {
        return generators;
    }

    public void setGenerators(Map<String, Generator> generators) {
        this.generators = generators;
    }

    public Map<String, Map<String, CollectionGenerator>> getCollectionGenerators() {
        return collectionGenerators;
    }

    public void setCollectionGenerators(Map<String, Map<String, CollectionGenerator>> collectionGenerators) {
        this.collectionGenerators = collectionGenerators;
    }

    public Generator getGeneratorForType(Class type) throws NoGeneratorForTypeException {
        if (!generators.containsKey(type.getName())) {
            throw new NoGeneratorForTypeException(type);
        }
        return generators.get(type.getName());
    }

    public CollectionGenerator getCollectionGeneratorForType(Class type, Class elementType)
            throws NoGeneratorForTypeException {
        if (!collectionGenerators.containsKey(type.getName())
                || !collectionGenerators.get(type.getName()).containsKey(elementType.getName())) {
            throw new NoGeneratorForTypeException(type);
        }

        return collectionGenerators
                .get(type.getName())
                .get(elementType.getName());
    }
}
