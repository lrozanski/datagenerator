package com.lrozanski.datagenerator.builder;

import com.lrozanski.datagenerator.Configuration;
import com.lrozanski.datagenerator.DataGenerator;

public class DataGeneratorBuilder {

    private Configuration configuration;

    public DataGeneratorBuilder() {
        this.configuration = getDefaultConfiguration();
    }

    public DataGeneratorBuilder withConfiguration(final Configuration configuration) {
        this.configuration = configuration;
        return this;
    }

    public DataGenerator build() {
        DataGenerator dataGenerator = new DataGenerator();
        dataGenerator.setConfiguration(configuration);
        return dataGenerator;
    }

    private Configuration getDefaultConfiguration() {
        return new ConfigurationBuilder()
                .withDefaultGenerators()
                .build();
    }
}
