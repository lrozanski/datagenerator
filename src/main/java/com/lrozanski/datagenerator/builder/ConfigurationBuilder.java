package com.lrozanski.datagenerator.builder;

import com.google.common.base.Preconditions;
import com.lrozanski.datagenerator.Configuration;
import com.lrozanski.datagenerator.exception.DataGeneratorException;
import com.lrozanski.datagenerator.generator.CollectionGenerator;
import com.lrozanski.datagenerator.generator.Generator;
import com.lrozanski.datagenerator.generator.basic.*;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class ConfigurationBuilder {

    private Map<String, Generator> generators = new HashMap<>();
    private Map<String, Map<String, CollectionGenerator>> collectionGenerators = new HashMap<>();

    public ConfigurationBuilder withDefaultGenerators() {
        Map<Class, Generator> generatorMap = new HashMap<>();
        generatorMap.put(Boolean.class, new BasicBooleanGenerator());
        generatorMap.put(Byte.class, new BasicByteGenerator());
        generatorMap.put(Integer.class, new BasicIntegerGenerator());
        generatorMap.put(Short.class, new BasicShortGenerator());
        generatorMap.put(Long.class, new BasicLongGenerator());
        generatorMap.put(Float.class, new BasicFloatGenerator());
        generatorMap.put(Double.class, new BasicDoubleGenerator());
        generatorMap.put(Character.class, new BasicCharacterGenerator());
        generatorMap.put(String.class, new BasicStringGenerator());
        generatorMap.put(Date.class, new BasicDateGenerator());
        generatorMap.put(LocalDate.class, new BasicLocalDateGenerator());
        generatorMap.put(LocalDateTime.class, new BasicLocalDateTimeGenerator());

        generators.put(boolean.class.getName(), generatorMap.get(Boolean.class));
        generators.put(byte.class.getName(), generatorMap.get(Byte.class));
        generators.put(int.class.getName(), generatorMap.get(Integer.class));
        generators.put(short.class.getName(), generatorMap.get(Short.class));
        generators.put(long.class.getName(), generatorMap.get(Long.class));
        generators.put(float.class.getName(), generatorMap.get(Float.class));
        generators.put(double.class.getName(), generatorMap.get(Double.class));
        generators.put(char.class.getName(), generatorMap.get(Character.class));

        generators.put(Boolean.class.getName(), generatorMap.get(Boolean.class));
        generators.put(Byte.class.getName(), generatorMap.get(Byte.class));
        generators.put(Integer.class.getName(), generatorMap.get(Integer.class));
        generators.put(Short.class.getName(), generatorMap.get(Short.class));
        generators.put(Long.class.getName(), generatorMap.get(Long.class));
        generators.put(Float.class.getName(), generatorMap.get(Float.class));
        generators.put(Double.class.getName(), generatorMap.get(Double.class));
        generators.put(Character.class.getName(), generatorMap.get(Character.class));

        generators.put(String.class.getName(), generatorMap.get(String.class));
        generators.put(Date.class.getName(), generatorMap.get(Date.class));
        generators.put(LocalDate.class.getName(), generatorMap.get(LocalDate.class));
        generators.put(LocalDateTime.class.getName(), generatorMap.get(LocalDateTime.class));

        // Collection generators
        populateCollectionGenerators(List.class, BasicListGenerator.class, generatorMap);
        populateCollectionGenerators(Set.class, BasicSetGenerator.class, generatorMap);

        return this;
    }

    public ConfigurationBuilder withGenerator(Class<?> type, Generator generator) {
        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(generator);

        generators.put(type.getName(), generator);
        return this;
    }

    public ConfigurationBuilder withCollectionGenerator(Class<?> type, Class<?> elementType, CollectionGenerator collectionGenerator) {
        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(elementType);
        Preconditions.checkNotNull(collectionGenerator);

        if (!collectionGenerators.containsKey(type.getName())) {
            collectionGenerators.put(type.getName(), new HashMap<>());
        }
        collectionGenerators.get(type.getName()).put(elementType.getName(), collectionGenerator);
        return this;
    }

    public Configuration build() {
        Configuration configuration = new Configuration();
        configuration.setGenerators(generators);
        configuration.setCollectionGenerators(collectionGenerators);
        return configuration;
    }

    private void populateCollectionGenerators(Class<? extends Collection> collectionClass,
                                              Class<? extends CollectionGenerator> collectionGeneratorClass,
                                              Map<Class, Generator> generatorMap) {
        collectionGenerators.put(collectionClass.getName(), new HashMap<>());

        collectionGenerators.get(collectionClass.getName()).put(boolean.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Boolean.class)));
        collectionGenerators.get(collectionClass.getName()).put(byte.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Byte.class)));
        collectionGenerators.get(collectionClass.getName()).put(int.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Integer.class)));
        collectionGenerators.get(collectionClass.getName()).put(short.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Short.class)));
        collectionGenerators.get(collectionClass.getName()).put(long.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Long.class)));
        collectionGenerators.get(collectionClass.getName()).put(float.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Float.class)));
        collectionGenerators.get(collectionClass.getName()).put(double.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Double.class)));
        collectionGenerators.get(collectionClass.getName()).put(char.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Character.class)));

        collectionGenerators.get(collectionClass.getName()).put(Boolean.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Boolean.class)));
        collectionGenerators.get(collectionClass.getName()).put(Byte.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Byte.class)));
        collectionGenerators.get(collectionClass.getName()).put(Integer.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Integer.class)));
        collectionGenerators.get(collectionClass.getName()).put(Short.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Short.class)));
        collectionGenerators.get(collectionClass.getName()).put(Long.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Long.class)));
        collectionGenerators.get(collectionClass.getName()).put(Float.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Float.class)));
        collectionGenerators.get(collectionClass.getName()).put(Double.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Double.class)));
        collectionGenerators.get(collectionClass.getName()).put(Character.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Character.class)));

        collectionGenerators.get(collectionClass.getName()).put(String.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(String.class)));
        collectionGenerators.get(collectionClass.getName()).put(Date.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(Date.class)));
        collectionGenerators.get(collectionClass.getName()).put(LocalDate.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(LocalDate.class)));
        collectionGenerators.get(collectionClass.getName()).put(LocalDateTime.class.getName(),
                createCollectionGenerator(collectionGeneratorClass, generatorMap.get(LocalDateTime.class)));
    }

    private <T> CollectionGenerator<T> createCollectionGenerator(Class<? extends CollectionGenerator> collectionGeneratorClass,
                                                                 Generator<T> generator) {
        try {
            return collectionGeneratorClass.getConstructor(Generator.class).newInstance(generator);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new DataGeneratorException(e);
        }
    }
}
