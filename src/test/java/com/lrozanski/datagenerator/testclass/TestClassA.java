package com.lrozanski.datagenerator.testclass;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class TestClassA {

    private Integer integerVar;
    private int primitiveIntegerVar;
    private Long longVar;
    private long primitiveLongVar;
    private Boolean booleanVar;
    private boolean primitiveBooleanVar;
    private Float floatVar;
    private float primitiveFloatVar;
    private Double doubleVar;
    private double primitiveDoubleVar;
    private String stringVar;
    private List<String> stringList;
    private Set<Boolean> primitiveBooleanSet;
    private Set<Boolean> booleanSet;
    private List<Character> characterList;
    private byte primitiveByteVar;
    private Byte byteVar;
    private short primitiveShortVar;
    private Short shortVar;
    private Character characterVar;
    private char primitiveCharacterVar;
    private Date dateVar;
    private LocalDate localDateVar;
    private LocalDateTime localDateTimeVar;
    private List<Date> dateList;
    private List<LocalDate> localDateList;
    private List<LocalDateTime> localDateTimeList;

    public Integer getIntegerVar() {
        return integerVar;
    }

    public void setIntegerVar(Integer integerVar) {
        this.integerVar = integerVar;
    }

    public Long getLongVar() {
        return longVar;
    }

    public void setLongVar(Long longVar) {
        this.longVar = longVar;
    }

    public long getPrimitiveLongVar() {
        return primitiveLongVar;
    }

    public void setPrimitiveLongVar(long primitiveLongVar) {
        this.primitiveLongVar = primitiveLongVar;
    }

    public int getPrimitiveIntegerVar() {
        return primitiveIntegerVar;
    }

    public void setPrimitiveIntegerVar(int primitiveIntegerVar) {
        this.primitiveIntegerVar = primitiveIntegerVar;
    }

    public Boolean getBooleanVar() {
        return booleanVar;
    }

    public void setBooleanVar(Boolean booleanVar) {
        this.booleanVar = booleanVar;
    }

    public boolean isPrimitiveBooleanVar() {
        return primitiveBooleanVar;
    }

    public void setPrimitiveBooleanVar(boolean primitiveBooleanVar) {
        this.primitiveBooleanVar = primitiveBooleanVar;
    }

    public Float getFloatVar() {
        return floatVar;
    }

    public void setFloatVar(Float floatVar) {
        this.floatVar = floatVar;
    }

    public float getPrimitiveFloatVar() {
        return primitiveFloatVar;
    }

    public void setPrimitiveFloatVar(float primitiveFloatVar) {
        this.primitiveFloatVar = primitiveFloatVar;
    }

    public Double getDoubleVar() {
        return doubleVar;
    }

    public void setDoubleVar(Double doubleVar) {
        this.doubleVar = doubleVar;
    }

    public double getPrimitiveDoubleVar() {
        return primitiveDoubleVar;
    }

    public void setPrimitiveDoubleVar(double primitiveDoubleVar) {
        this.primitiveDoubleVar = primitiveDoubleVar;
    }

    public String getStringVar() {
        return stringVar;
    }

    public void setStringVar(String stringVar) {
        this.stringVar = stringVar;
    }

    public List<String> getStringList() {
        return stringList;
    }

    public void setStringList(List<String> stringList) {
        this.stringList = stringList;
    }

    public Set<Boolean> getPrimitiveBooleanSet() {
        return primitiveBooleanSet;
    }

    public void setPrimitiveBooleanSet(Set<Boolean> primitiveBooleanSet) {
        this.primitiveBooleanSet = primitiveBooleanSet;
    }

    public Set<Boolean> getBooleanSet() {
        return booleanSet;
    }

    public void setBooleanSet(Set<Boolean> booleanSet) {
        this.booleanSet = booleanSet;
    }

    public List<Character> getCharacterList() {
        return characterList;
    }

    public void setCharacterList(List<Character> characterList) {
        this.characterList = characterList;
    }

    public byte getPrimitiveByteVar() {
        return primitiveByteVar;
    }

    public void setPrimitiveByteVar(byte primitiveByteVar) {
        this.primitiveByteVar = primitiveByteVar;
    }

    public Byte getByteVar() {
        return byteVar;
    }

    public void setByteVar(Byte byteVar) {
        this.byteVar = byteVar;
    }

    public short getPrimitiveShortVar() {
        return primitiveShortVar;
    }

    public void setPrimitiveShortVar(short primitiveShortVar) {
        this.primitiveShortVar = primitiveShortVar;
    }

    public Short getShortVar() {
        return shortVar;
    }

    public void setShortVar(Short shortVar) {
        this.shortVar = shortVar;
    }

    public Character getCharacterVar() {
        return characterVar;
    }

    public void setCharacterVar(Character characterVar) {
        this.characterVar = characterVar;
    }

    public char getPrimitiveCharacterVar() {
        return primitiveCharacterVar;
    }

    public void setPrimitiveCharacterVar(char primitiveCharacterVar) {
        this.primitiveCharacterVar = primitiveCharacterVar;
    }

    public Date getDateVar() {
        return dateVar;
    }

    public void setDateVar(Date dateVar) {
        this.dateVar = dateVar;
    }

    public LocalDate getLocalDateVar() {
        return localDateVar;
    }

    public void setLocalDateVar(LocalDate localDateVar) {
        this.localDateVar = localDateVar;
    }

    public LocalDateTime getLocalDateTimeVar() {
        return localDateTimeVar;
    }

    public void setLocalDateTimeVar(LocalDateTime localDateTimeVar) {
        this.localDateTimeVar = localDateTimeVar;
    }

    public List<Date> getDateList() {
        return dateList;
    }

    public void setDateList(List<Date> dateList) {
        this.dateList = dateList;
    }

    public List<LocalDate> getLocalDateList() {
        return localDateList;
    }

    public void setLocalDateList(List<LocalDate> localDateList) {
        this.localDateList = localDateList;
    }

    public List<LocalDateTime> getLocalDateTimeList() {
        return localDateTimeList;
    }

    public void setLocalDateTimeList(List<LocalDateTime> localDateTimeList) {
        this.localDateTimeList = localDateTimeList;
    }
}
