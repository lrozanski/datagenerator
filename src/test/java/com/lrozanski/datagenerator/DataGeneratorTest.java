package com.lrozanski.datagenerator;

import com.lrozanski.datagenerator.builder.ConfigurationBuilder;
import com.lrozanski.datagenerator.builder.DataGeneratorBuilder;
import com.lrozanski.datagenerator.generator.basic.BasicListGenerator;
import com.lrozanski.datagenerator.testclass.TestClassA;
import com.lrozanski.datagenerator.testclass.TestClassB;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

public class DataGeneratorTest {

    @Test
    public void generateForClass() {
        // GIVEN
        final Configuration configuration = new ConfigurationBuilder()
                .withDefaultGenerators()
                .withGenerator(LocalDate.class, seed -> LocalDate.now().plusDays(new Random().nextInt(11)))
                .withCollectionGenerator(List.class, LocalDateTime.class, new BasicListGenerator<>(
                        seed -> LocalDateTime.now().minusDays(new Random().nextInt(11)), () -> 1
                ))
                .build();

        final DataGenerator dataGenerator = new DataGeneratorBuilder()
                .withConfiguration(configuration)
                .build();

        final TestClassA testClassA = dataGenerator.generateForClass(TestClassA.class);
        final TestClassB testClassB = new TestClassB();

        // WHEN
        dataGenerator.generateForClass(testClassB, TestClassB.class);

        // THEN
        Assertions.assertThat(testClassA).isNotNull();
        Assertions.assertThat(testClassB).isNotNull();
    }
}
